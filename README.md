# Shinesoftware InventoryFix

Reorder feature in the administration page thrown an error when a product is out of stock. For more details look at https://github.com/magento/magento2/issues/32651

    ``shinesoftware/module-inventoryfix``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Fix for the inventory of magento

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Shinesoftware`
 - Enable the module by running `php bin/magento module:enable Shinesoftware_InventoryFix`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

## Fix Specifications

- Reorder feature in the administration page thrown an error when a product is out of stock #32651 (https://github.com/magento/magento2/issues/32651)